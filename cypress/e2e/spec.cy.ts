describe('My First Test', () => {
  it('Visits the initial project page', () => {
    cy.intercept('GET', '/assets/markdown/home.md', {
      statusCode: 200,
      body: '# Objectives',
    });
    cy.visit('/');
    cy.get('h1').contains('Objectives');
    cy.get('[data-cy="product"]').click();
  });

  it('Visit the login page', () => {
    cy.visit('/');
    cy.get('[data-cy="login"]').click();
    cy.url().should('contain', '/login');
  });

  it('Visit dashboard will redirect to login', () => {
    cy.visit('/dashboard');
    cy.url().should('contain', '/login');
  });

  it('Visit dashboard with login token', () => {
    cy.authen();

    cy.visit('/dashboard');
    cy.url().should('contain', '/dashboard');
  });
});
